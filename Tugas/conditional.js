console.log('================================');
console.log('Tugas Conditional');
console.log('================================');
var nama = "Jhon"
var peran = ""
if (nama == ""){
    console.log("Welcome to Werewolf")
} else {
    console.log("Nama Harus Diisi")
}
if ( nama == "Jhon") {
    console.log("Hallo Jhon Pilih peran mu untuk Memulai Game")
} else {
    console.log("Memulai Game")
}

var nama = "Jane"
var peran = "Penyihir"
if (nama == "Jane"){
    console.log("Selamat Datang Ke Dunia WErewolf, Jane")
} else {
    console.log("Nama Harus Diisi")
}
if ( peran == "Penyihir") {
    console.log("Hallo Penyihir Jane, Kamu dapat melihat siapa yang menjadi werewolf")
} else {
    console.log("tidak dapat melihat siapa yang menjadi werewolf")
}


var nama = "Jenita"
var peran = "Guard"
if (nama == "Jenita"){
    console.log("Selamat Datang Ke Dunia WErewolf, Jenita")
} else {
    console.log("Nama Harus Diisi")
}
if ( peran == "Guard") {
    console.log("Hallo Guard Jenita, Kamu dapat membantu temanmu dari serangan werewolf")
} else {
    console.log("tidak dapat melindung dari werewolf")
}

var nama = "Junaedi"
var peran = "Werewolf"
if (nama == "Junaedi"){
    console.log("Selamat Datang Ke Dunia WErewolf, Junaedi")
} else {
    console.log("Nama Harus Diisi")
}
if ( peran == "Werewolf") {
    console.log("Hallo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("tidak dapat memangsa")
}