console.log('================================');
console.log('Tugas Switch');
console.log('================================');
var tanggal = 1;
switch(tanggal){
    case 1 : { console.log('Selasa Tanggal 1'); break ; }
    case 2 : { console.log('Rabu Tanggal 2'); break ; }
    case 3 : { console.log('Kamis Tanggal 3'); break ; }
    case 4 : { console.log('Jumat Tanggal 4'); break ; }
    case 5 : { console.log('Sabtu Tanggal 5'); break ; }
    case 6 : { console.log('Minggu Tanggal 6'); break ; }
    default : { console.log('tidak terjadi apa-apa'); }}

var bulan = 6;
switch(bulan){
    case 1 : { console.log(' January'); break ; }
    case 2 : { console.log(' February'); break ; }
    case 3 : { console.log('Maret'); break ; }
    case 4 : { console.log('April'); break ; }
    case 5 : { console.log('Mei'); break ; }
    case 6 : { console.log('Juni'); break ; }
    default : { console.log('tidak terjadi apa-apa'); }}

var tahun = 1996;
switch(tahun){
    case 1996 : { console.log('Tahun 1996'); break ; }
    case 2000 : { console.log('Tahun 2000'); break ; }
    case 2001 : { console.log('Tahun 2001'); break ; }
    case 1997 : { console.log('Tahun 1997'); break ; }
    case 2003 : { console.log('Tahun 2003'); break ; }
    case 2200 : { console.log('Tahun 2020'); break ; }
    default : { console.log('tidak terjadi apa-apa'); }}